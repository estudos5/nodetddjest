# Testes no NodeJS aplicando TDD com Jest | Diego Fernandes

Jest, Factory Girl, Faker, Supertest, Sequelize, JWT, BCrypt

```
$ yarn add express
```

# Banco de dados

Usando um banco de daos relacional Posgrees e o ORM Sequelize.
Instalar o pacote pg para o sequelize se comunicar com o Posgrees

```
$ yarn add sequelize pg
```
Instalar o sequelize-cli para ajudar na criação de migrations(arquivos que controlam as versões das tabelas no banco de dados)

```
$ yarn add sequelize-cli -D
```
Rodar o comando do sequelize para iniciar os arquivos
```
$ yarn sequelize init
```

## Container Postgrees
instanciando um [container postgres]()
```
$ docker run -p 5432:5432\
    --name postgres \
    -e POSTGRES_USER=docker \
    -e POSTGRES_PASSWORD=docker \
    -d postgres
```
testando o container
```
$ docker exec -it postgres bash
# psql -U docker -W
```
Criar instancia do [pgadmin4 container](https://www.pgadmin.org/docs/pgadmin4/latest/container_deployment.html)
```
docker run -p 80:80 \
    --name pgadmin4 \
    --link postgres \
    -e 'PGADMIN_DEFAULT_EMAIL=docker' \
    -e 'PGADMIN_DEFAULT_PASSWORD=docker' \
    -d dpage/pgadmin4
```