module.exports = {
  host: '127.0.0.1',
  username: 'docker',
  password: 'docker',
  database: 'nodeauth',
  dialect: 'postgres',
  operatorsAliases: false, // desabilitar um warning do sequelize
  logging: false, // não mostrar muitos logs enquanto está rodando as migrations
  define: {
    timestamps: true, // toda tabela tem os campos created_at e updated_at
    underscored: true, // cria tabela com snake_case
    underscoredAll: true // cria todos os campos das tabelas com snake_case
  }
}